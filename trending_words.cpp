#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include <unordered_map>
#include <algorithm>

#include "common.h"
///#include "priority_queue.h"

using namespace std;

struct TrendingWord
{
    TrendingWord(std::string w) : word(w), count(1) {}
    std::string word;
    unsigned int count;

    bool operator<(const TrendingWord& other) const { return count < other.count; }
    bool operator>(const TrendingWord& other) const { return count > other.count; }

    friend ostream& operator<<(ostream& out, const TrendingWord& w)
    {
        out << w.word << " (" << w.count << ")";
        return out;
    }

};

typedef shared_ptr<TrendingWord> TrendingWordPtr;

bool cmp(const TrendingWordPtr& lhs, const TrendingWordPtr& rhs)
{
    return lhs->count < rhs->count;
}

ostream& operator<<(ostream& out, const TrendingWordPtr& w)
{
    out << *w;
    return out;
}

//typedef PriorityQueue<TrendingWordPtr, std::vector<TrendingWordPtr>, std::greater<TrendingWordPtr>> PQueue;
typedef std::vector<TrendingWordPtr> PQueue;

template<typename Heap>
int get_top_words(Heap heap, int k, vector<string>& words)
{
    int i =0;
    for(; i<k && !heap.empty(); i++)     //O(k)
    {
        make_heap(heap.begin(), heap.end(), cmp); // O(3N)
        words[i] = heap.front()->word;
        pop_heap(heap.begin(), heap.end());
        heap.pop_back();
    }
    return i;
}

void trending_words(int k = 3)
{
    unordered_map<string, TrendingWordPtr> words;
    PQueue pqueue;

    string word;
    vector<string> top_words(k);
    while(cin >> word)
    {
        auto it = words.find(word);
        if(it == words.end())
        {
            auto newWord = make_shared<TrendingWord>(word);
            words[word] = newWord;
            pqueue.push_back(newWord);
        }
        else
        {
            it->second->count += 1;
        }

        // slowww! <- need priority_queue.increase_key(...) instead of rebuilding
        //int n = get_top_words(pqueue, k, top_words);
        //for(int i=0; i<n; i++)
        //    cout << words[top_words[i]]->count << " " << words[top_words[i]]->word << ",";
        //cout << endl;
    }

    int n = get_top_words(pqueue, k, top_words);
    for(int i=0; i<n; i++)
        cout << words[top_words[i]]->count << " " << words[top_words[i]]->word << endl;

}
int main(int argc, char* argv[])
{
    int k = 3;
    if(argc > 1) k = std::atoi(argv[1]);
    trending_words(k);

    return 0;
}
