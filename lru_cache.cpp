#include <iostream>
#include <algorithm>
#include <string>
#include <memory>
#include <cassert>
#include <list>
#include <vector>
#include <stdexcept>
#include <functional>
#include <unordered_map>

template<typename Key, typename T>
class LRUcache
{
public:
    LRUcache(size_t max_items): max_items(max_items) {}

    void put(const Key& key, const T&t)
    {
        if(list.size() >= max_items) prune();
        if(map.find(key) == map.end()) map[key] = t;
        promote(key);
    }

    T get(const Key& key)
    {
        auto it = map.find(key);
        if(it == map.end()) throw std::runtime_error("key not found: '" + key + "'");

        promote(key);
        return it->second;
    }

    void promote(const Key& key)
    {
        auto it = std::find(list.begin(), list.end(), key);
        if(it != list.end()) list.erase(it);
        list.push_front(key);
    }

    void prune()
    {
        auto firstKey = list.begin();
        std::advance(firstKey, max_items-1);

        for(auto key = firstKey; key != list.end(); ++key)
        {
            map.erase(*key);
        }

        std::list<Key> newList;
        auto lastKey = list.begin();
        std::advance(lastKey, max_items-1);
        newList.splice(newList.begin(), list, list.begin(), lastKey);
        std::swap(list, newList);
    }

    friend std::ostream& operator<<(std::ostream& out, const LRUcache& cache)
    {
        for(auto it = cache.list.begin(); it != cache.list.end(); ++it)
        {
            auto v = cache.map[*it];
            out << *it << ":" << v << ", "; 
        }
        return out;
    }

private:
    size_t                                  max_items;
    mutable std::unordered_map<Key, T>      map;
    std::list<Key>                          list;
};

int main(int argc, char* argv[])
{
    using namespace std;
    LRUcache<string, int> cache(2);

    cache.put("one", 1);
    cache.put("two", 2);
    cache.put("three", 3);
    cout << cache << endl;

    cache.get("two");
    cache.get("two");
    cache.get("two");
    cache.get("three");
    cache.get("three");
    cache.get("two");

    cache.put("one", 1);
    cache.get("one");

    cout << cache << endl;

}

