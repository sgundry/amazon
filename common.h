#include <utility>
#include <vector>
#include <chrono>
#include <iostream>
#include <sstream>

typedef std::pair<size_t, size_t> Range;

template<typename T>
std::vector<T> make_vector(const std::initializer_list<T> values)
{
    return std::vector<T>(values);
}

std::vector<int> make_random_vector(int N, int lo = 0, int hi = 100)
{
    std::vector<int> nums;
    nums.reserve(N);
    for(int i=0; i<N; i++) nums.push_back(lo + (hi - rand() % lo));
    return nums;
}

std::string print(const std::vector<int>& v, size_t begin, size_t end)
{
    std::stringstream ss;
    //std::cout << "print(" << begin << "," << end << ")" << std::endl;
    for(size_t i=begin; i<end; i++) 
        ss << v[i] << ",";
    return ss.str();
}

int randint(int lo, int hi)
{
    return rand() % (hi-lo) + lo;
}

template<typename Container>
void print(const Container& v)
{
    for(auto n = v.begin(); n != v.end(); ++n) std::cout << *n << ",";
    std::cout << std::endl;
}

bool is_sorted(const std::vector<int>& v, size_t begin, size_t end)
{
    for(size_t i=begin+1; i<end; i++)
    {
        if(v[i] < v[i-1]) return false;
    }
    return true;
}

bool is_sorted(const std::vector<int>& v)
{
    return is_sorted(v, 0, v.size());
}

struct Timer
{
    std::chrono::time_point<std::chrono::system_clock> start, end;

    Timer(): start(std::chrono::system_clock::now()) {}

    double elapsed() const
    { 
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        return elapsed_seconds.count();
    };

};
