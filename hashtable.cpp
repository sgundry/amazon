#include <iostream>
#include <string>
#include <memory>
#include <cassert>
#include <list>
#include <vector>
#include <stdexcept>
#include <functional>

template<typename Key, typename T>
class Dict
{
public:
    Dict() : buckets(initial_bucket_size), nkeys(0), nbuckets_filled(0), current_load_factor(0.0f)
    {
    }

    void insert(const Key& key, const T& t)
    {
        auto hash_value = std::hash<Key>()(key);
        auto& bucket = hash_to_bucket(hash_value);

        ++nkeys;
        if(bucket.empty()) 
        {
            bucket.key_values.push_back(std::make_pair(key,t));
            current_load_factor = nkeys / static_cast<float>(buckets.capacity());

            if(current_load_factor > max_load_factor) 
            {
                std::cout << "current_load_factor=" << current_load_factor << " nbuckets_filled=" << nbuckets_filled << " - resizing from " << buckets.capacity() << " to " << buckets.capacity()*2 << std::endl;
                resize(buckets.capacity()*2);
            }
        }
        else 
        {
            //std::cout << "collision (" << key << " " << t << ") bucket.size=" << bucket.key_values.size()+1 << std::endl;
            bucket.key_values.push_back(std::make_pair(key,t));
        }
    }

    T find(const Key& key) const 
    {
        auto& bucket = hash_to_bucket(std::hash<Key>()(key));

        if(bucket.empty()) 
            throw std::runtime_error("key not found");

        auto it = bucket.key_values.begin();
        for(; it != bucket.key_values.end() && it->first != key; ++it);

        if(it == bucket.key_values.end())
            throw std::runtime_error("key not found");

        return it->second;
    }

    bool exists(const Key& key) const
    {
        try
        {
            find(key);
            return true;
        }
        catch (const std::runtime_error& e) 
        {
            return false;
        }
    }

    void remove(const Key& key) 
    {
        auto& bucket = hash_to_bucket(std::hash<Key>()(key));

        if(bucket.empty()) return;

        auto it = bucket.key_values.begin();
        for(; it != bucket.key_values.end() && it->first != key; ++it);

        if(it == bucket.key_values.end()) return;

        std::cout << "erasing " << key << " it=" << it->first << "," << it->second <<  std::endl;
        bucket.key_values.erase(it);
    }

    T operator[](const Key& key) const { return find(key); }

    size_t num_keys() const { return nkeys; }
    size_t num_buckets() const { return buckets.capacity(); }
    size_t num_buckets_filled() const { return nbuckets_filled; }

    float average_bucket_load() const 
    {
        int sum = 0;
        for(auto bucket = buckets.begin(); bucket != buckets.end(); ++bucket) sum += bucket->key_values.size();
        return sum / float(buckets.capacity());
    }

    float get_max_load_factor() const { return max_load_factor; }

private:
    struct Bucket
    {
        bool empty() const { return key_values.empty(); }
        std::list<std::pair<Key,T>> key_values;
    };

    const Bucket& get_bucket(const Key& key) const { hash_to_bucket(std::hash<Key>()(key)); }

    void resize(size_t bucket_size)
    {
        nkeys = 0;
        nbuckets_filled = 0;

        auto old_buckets = std::move(buckets);
        buckets = std::vector<Bucket>(bucket_size); 
        for(auto bucket = old_buckets.begin(); bucket != old_buckets.end(); ++bucket)
        {
            for(auto kv = bucket->key_values.begin(); kv != bucket->key_values.end(); ++kv)
            {
                insert(kv->first, kv->second);
            }
        }
    }

    const Bucket& hash_to_bucket(size_t hash_value) const
    { 
        auto bucket_index = hash_value % buckets.size();
        return buckets[bucket_index]; 
    }

    Bucket& hash_to_bucket(size_t hash_value)
    { 
        auto bucket_index = hash_value % buckets.capacity();
        return buckets[bucket_index]; 
    }

    static const auto initial_bucket_size = 1000;
    static const auto max_load_factor = 5.0f;

    std::vector<Bucket> buckets;
    size_t nkeys;
    size_t nbuckets_filled;
    float current_load_factor;
};

int main(int argc, char* argv[])
{
    using namespace std;
    Dict<string, int> dict;

    auto nkeys = 10000;
    for(int i=1; i<nkeys; i++) 
    {
        dict.insert("number_" + std::to_string((long long)i), i);
    }

    std::cout << dict["number_666"] << std::endl;
    dict.remove("number_666");
    assert(!dict.exists("number_666"));
    
    std::cout << "nkeys=" << dict.num_keys() << " nbuckets=" << dict.num_buckets() << " avg bucket load=" << dict.average_bucket_load() << " (max=" << dict.get_max_load_factor() << ")" << std::endl;
}

