#include <iostream>
#include <string>
#include <memory>
#include <cassert>
#include <list>
#include <vector>
#include <queue>
#include <set>
#include <limits>
#include <algorithm>

#include "common.h"

using namespace std;

class Graph
{
public:
    struct Edge
    {
        int v;
        int weight;
    };

    struct Node
    {
        int index;
        std::list<Edge> edges; 
    };

    //typedef std::vector<std::list<int>> AdjacencyList;
    typedef std::vector<Node*> AdjacencyList;

    Graph(int nvertices) : nverts(nvertices), nodes(nvertices) {}

    ~Graph()
    {
        for(int i=0; i<nodes.size(); i++) delete nodes[i];
    }

    void insert(int u, const std::initializer_list<int>& adjacent_nodes)
    {
        auto* node = new Node;
        node->index = u;
        for(auto v=adjacent_nodes.begin(); v != adjacent_nodes.end(); ++v)
        {
            Edge e = {*v, 1};
            node->edges.push_back(e);
        }

        nodes[u] = node;
    }

    void insert_with_weights(int u, const std::initializer_list<std::pair<int, int>>& edges)
    {
        auto* node = new Node;
        node->index = u;
        for(auto it=edges.begin(); it != edges.end(); ++it)
        {
            Edge e = {it->first, it->second};
            node->edges.push_back(e);
        }

        nodes[u] = node;
    }

    void topo_sort()
    {
        std::vector<int> indegree(nverts, 0);
        for(int u=0; u<nverts; u++)
        {
            auto* node = nodes[u];
            for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
            {
                indegree[e->v] += 1;
            }
        }

        std::queue<int> q;
        q.push(0);
        while(!q.empty())
        {
            int u = q.front(); q.pop();
            auto* node = nodes[u];
            cout << "visiting u=" << u << endl;
            for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
            {
                if(--indegree[e->v] == 0)
                    q.push(e->v);
            }
        }
    }

    void dfs()
    {
        std::set<int> seen;
        dfs(0, seen);
        assert(seen.size() == nverts);
    }

    void dfs(int u, std::set<int>& seen)
    {
        cout << "visiting u=" << u << endl;

        seen.insert(u);
        auto *node = nodes[u];
        for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
        {
            if(seen.find(e->v) == seen.end())
                dfs(e->v, seen);
        }
    }

    std::vector<int> unweighted_shortest_path(int s, int t)
    {
        auto infinity = std::numeric_limits<int>::max();
        std::vector<int>    dist(nverts, infinity);
        std::queue<int>     q;
        std::vector<int>    path(nverts, -1);
        q.push(s);
        dist[s] = 0;

        while(!q.empty())
        {
            int u = q.front(); q.pop();
            auto* node = nodes[u];
            for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
            {
                if(dist[e->v] == infinity) 
                {
                    dist[e->v] = 1 + dist[u];
                    path[e->v] = u;
                    q.push(e->v);
                }
            }
        }

        std::list<int> shortest_path;
        int u = t;
        while(u != s)
        {
            shortest_path.push_front(u);
            u = path[u];
        }
        shortest_path.push_front(s);

        return std::vector<int>(shortest_path.begin(), shortest_path.end());
    }

    std::pair<std::vector<int>, int> 
    weighted_shortest_path(int s, int t)
    {
        auto infinity = std::numeric_limits<int>::max();
        std::vector<int>    dist(nverts, infinity);
        std::vector<int>    path(nverts, -1);
        std::vector<int>    known(nverts, false);
    
        dist[s] = 0;

        // for each node u
        int count = 0;
        while(true)
        {
            // find smallest unknown d 
            int u = -1;
            int min_dist = infinity;
            for(int j=0; j<nverts; j++) 
            {
                if(!known[j] && dist[j] < min_dist)
                {
                    u = j;
                    min_dist = dist[j];
                }
            }

            if(u == -1) break;

            cout << "node u=" << u << endl;
            known[u] = true;

            //  for each adjacent node v
            auto* node = nodes[u];
            for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
            {
                if(!known[e->v]) 
                {
                    if( dist[u] + e->weight < dist[e->v])
                    {
                        dist[e->v] = dist[u] + e->weight;
                        path[e->v] = u;
                    }
                }
            }
        }

        std::list<int> shortest_path;
        int u = t;
        int cost = 0;
        while(u != s)
        {
            shortest_path.push_front(u);
            u = path[u];
        }
        shortest_path.push_front(s);

        return {std::vector<int>(shortest_path.begin(), shortest_path.end()), dist[t]};
    }

    std::pair<std::vector<int>, int> 
    weighted_shortest_path_minheap(int s, int t)
    {
        auto infinity = std::numeric_limits<int>::max();
        std::vector<int>        dist(nverts, infinity);
        std::vector<int>        path(nverts, -1);
    

        // compare for priority queue
        struct Compare
        {
            Compare(std::vector<int>& dist): dist(dist) {}
            bool operator()(const Node*& u, const Node*& v) const { return dist[u->index] > dist[v->index]; }
            std::vector<int>& dist;
        };

        Compare cmp(dist);
        std::vector<Node*>      queue;
        std::make_heap(queue.begin(), queue.end());

        queue.push_back(nodes[s]);
        std::push_heap(queue.begin(), queue.end());
        dist[s] = 0;

        while(!queue.empty())
        {
            // get next closest node
            std::pop_heap(queue.begin(), queue.end());
            auto* node = queue.back();
            queue.pop_back();
            auto u = node->index;

            //  for each adjacent node v
            for(auto e=node->edges.begin(); e != node->edges.end(); ++e)
            {
                if( dist[u] + e->weight < dist[e->v])
                {
                    dist[e->v] = dist[u] + e->weight;
                    path[e->v] = u;
                    queue.push_back(nodes[e->v]);
                    std::push_heap(queue.begin(), queue.end());
                }
            }
        }

        std::list<int> shortest_path;
        int u = t;
        int cost = 0;
        while(u != s)
        {
            shortest_path.push_front(u);
            u = path[u];
        }
        shortest_path.push_front(s);

        return {std::vector<int>(shortest_path.begin(), shortest_path.end()), dist[t]};
    }

    int                 nverts;
    AdjacencyList       nodes;
};

int main(int argc, char* argv[])
{
    using namespace std;

    Graph g(7);
    
    g.insert(0, {1,2});
    g.insert(1, {3,4});
    g.insert(2, {3});
    g.insert(3, {4,5});
    g.insert(4, {6});
    g.insert(5, {6});
    g.insert(6, {});

    cout << "topo sort: " << endl;
    g.topo_sort();

    cout << "depth first search: " << endl;
    g.dfs();

    auto path = g.unweighted_shortest_path(2,6);
    cout << "distance(2,6)=" << path.size() << endl;
    print(path);

    {
        // From Weiss, 2nd ed. page 290
        Graph g(7);
        g.insert_with_weights(0, {{1,2},{3,1}});
        g.insert_with_weights(1, {{3,3},{4,10}});
        g.insert_with_weights(2, {{0,4},{5,5}});
        g.insert_with_weights(3, {{2,2},{4,2},{5,8},{6,4}});
        g.insert_with_weights(4, {{6,6}});
        g.insert_with_weights(5, {});
        g.insert_with_weights(6, {{5,6}});

        {
            std::vector<int> path;
            int cost;
            tie(path, cost) = g.weighted_shortest_path(0,5);
            cout << "distance(0,5)=" << cost << endl;
            print(path);
        }
        {
            std::vector<int> path;
            int cost;
            tie(path, cost) = g.weighted_shortest_path(0,5);
            cout << "distance_minheap(0,5)=" << cost << endl;
            print(path);
        }
    }

}
