#include <iostream>
#include <string>
#include <array>
#include <deque>
#include <algorithm>
#include <vector>
#include <set>
#include <fstream>
#include <utility>
#include <tuple>
#include <queue>
#include <cassert>
#include <sstream>
#include <map>
#include <cstring>
 
bool isAnagram(const std::string& s, const std::string& t)
{
    if(s.length() != t.length()) return false;
 
    std::array<int,256> count;
    std::fill(count.begin(), count.end(), 0);
 
    for(int i=0; i<s.length(); i++) {
        count[s[i]]++;
        count[t[i]]--;
    }
 
    for(int i=0; i<count.size(); i++) {
        if(count[i] != 0) return false;
    }
 
    return true;
}
 
char firstNonRepeatedChar(std::string s)
{
    // yellow = y
    // tooth = h
    auto N = s.length();
    std::vector<bool> uniq(128, false);
 
    for(int i=0; i<N; i++) uniq[s[i]] ? uniq[s[i]] = false : uniq[s[i]] = true;
    for(int i=0; i<N; i++) if(uniq[s[i]] == true) return s[i];
 
    return ' ';
}
 
std::set<std::string> load_dict(const std::string& filename)
{
    std::set<std::string> dict;
 
    std::ifstream file;
    file.open(filename);
    while (!file.eof())
    {
        std::string line;
        getline(file, line);
        if (line.length() > 0)
        {
            dict.insert(line);
        }
    }
 
    return dict;
}
 
std::string words_string(std::deque<std::string> words)
{
    std::stringstream ss;
    for(auto w = words.begin(); w != words.end(); ++w)
    {
        ss << *w << " ";
    }
    auto s = ss.str();
    return s.substr(0,s.size()-1);
}
 
 
 
typedef std::set<std::string> Dict;
typedef std::map<std::string, std::string> Cache;
 
bool contains(const Dict& dict, const std::string& w) { return dict.find(w) != dict.end(); }
 
std::string split(const Dict& dict, Cache& cache, const std::string& text)
{
    using namespace std;

    if(contains(dict, text)) return text;

    auto cached = cache.find(text);
    if(cached != cache.end()) 
    {
        cout << "cache hit text=" << text << " cache=" << cached->second << endl;
        return cached->second;
    }

    auto N = text.size();
    for(size_t i=1; i<N; i++)
    {
        auto prefix = text.substr(0,i);
        if(contains(dict, prefix))
        {
            cache[text] = prefix;
            auto suffix = text.substr(i,N); 
            auto next = split(dict, cache, suffix);
            if(next != "")
            { 
                return prefix + " " + next;
            }
        }
    }
    
    return "";
}

std::string split(const Dict& dict, const std::string& text)
{
    Cache cache;
    auto s = split(dict, cache, text);
    std::cout << text << "=" << s << std::endl;
    return s;
}
 
// recursive time = O(2^N) worst case (if every letter is different)
int lcs(const std::string& x, const std::string& y, std::string& max_substring, std::string substring = "" )
{
    int M = x.size();
    int N = y.size();

    if(M == 0 || N == 0) 
    {
        if(substring.size() > max_substring.size())
        {
            max_substring = substring;
        }
        return 0;
    }

    if(x[M-1] == y[N-1])
    {
        substring += x[M-1];
        return 1 + lcs(x.substr(0,M-1), y.substr(0,N-1), max_substring, substring);
    }
    else 
    {
        return std::max(lcs(x.substr(0,M-1), y, max_substring, substring), lcs(x, y.substr(0,N-1), max_substring, substring));
    }
}

int lcs(const std::string& x, const std::string& y)
{
    std::string s;
    int max_length = lcs(x,y,s);

    std::reverse(s.begin(), s.end());
    std::cout << "lcs: " << s << std::endl;
    return max_length;
}

int count( int S[], int m, int n )
{
    // table[i] will be storing the number of solutions for
    // value i. We need n+1 rows as the table is consturcted
    // in bottom up manner using the base case (n = 0)
    int table[n+1];
 
    // Initialize all table values as 0
    memset(table, 0, sizeof(table));
 
    // Base case (If given value is 0)
    table[0] = 1;
 
    // Pick all coins one by one and update the table[] values
    // after the index greater than or equal to the value of the
    // picked coin
    for(int i=0; i<m; i++)
        for(int j=S[i]; j<=n; j++)
            table[j] += table[j-S[i]];
 
    return table[n];
}


int main(int argc, char* argv[])
{
    using namespace std;

    int arr[] = {1, 2, 3};
    int m = sizeof(arr)/sizeof(arr[0]);
    int n = 4;

    cout << count(arr, m, n) << endl;;

    return 0;

    cout << lcs("AGGTAB", "GXTXAYB") << endl;
 
    return 0;
    //auto dict = load_dict("/usr/share/dict/words");
    auto dict = load_dict("./words");
 
    auto s = string("togetheragain");
    cout << s << "=" << split(dict, s) << endl;
 
    return 0;
}
