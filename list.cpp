#include <iostream>
#include <string>
#include <memory>
#include <cassert>

template<typename T>
class List
{
public:
    struct Node
    {
        Node(const T& t): value(t), next(NULL), prev(NULL) {}
        T value;
        Node* next;
        Node* prev;
    };

    List(std::initializer_list<T> values): head(new Node(-1)), m_size(0)
    {
        for(auto v = values.begin(); v != values.end(); ++v)
        {
            push_back(*v);
        }
    }

    ~List()
    {
        auto* n = head->next;
        while(n)
        {
            auto* tmp = n;
            n = tmp->next;
            delete tmp;
        }
        delete head;
    }

    /// add a node with value t after node prev
    /// O(1)
    void insert(Node* n, const T& t)
    {
        assert(n);
        auto* newNode = new Node(t);
        if(n->next) newNode->next = n->next;

        newNode->prev = n;
        n->next = newNode;
        ++m_size;
    }

    // add node n after prev (supports creating a loop!)
    void insert(Node* prev, Node* n) 
    {
        n->next = prev->next;
        if(n->next) n->next->prev = n;

        n->prev = prev;
        prev->next = n;

        assert(prev->next == n);
        assert(n->prev == prev);
        assert(n->next->prev == n);
    }

    size_t size() const { return m_size; }

    // O(n)
    Node* last() const 
    { 
        auto* n = head;
        while(n && n->next) n = n->next;
        return n;
    }

    void push_back(const T& t)
    {
        insert(last(), t);
    }

    Node* find(const T& t)
    {
        auto* n = head->next;
        while(n && n->value != t) n = n->next;
        if(n && n->value == t) return n;
        else return NULL;
    }

    bool remove(const T& t)
    {
        if(auto* n = find(t)) 
        {
            auto* prev = n->prev;
            if(prev) prev->next = n->next;
            delete n;
            --m_size;
            return true;
        }

        return false;
    }

    void remove(Node* n)
    {
        assert(n);

        auto* prev = n->prev;
        if(prev) prev->next = n->next;
        if(prev->next) prev->next->prev = prev;
        delete n;
        --m_size;
    }

    bool has_loop() const 
    {
        Node* n1 = head;
        Node* n2 = head;
        while(n1 && n2)
        {
            std::cout << "n1=" << n1->value << " n2=" << n2->value << std::endl; 
            n1 = n1->next;
            n2 = n2->next->next;
            if(n1 == n2) return true;
        }

        return false;
    }

    friend std::ostream& operator<<(std::ostream& out, const List& list)
    {
        auto* n = list.head->next;
        while(n && n->next)
        {
            out << n->value << " -> ";
            n = n->next;
        }
        if(n) out << n->value;
        return out;
    }

private:
    Node*       head;         // sentinal 
    size_t      m_size;
};

int main(int argc, char* argv[])
{
    using namespace std;

    List<int> nums{1,2,3,4,5};
    std::cout << nums << std::endl;
    assert(nums.size() == 5);

    nums.push_back(6);
    assert(nums.size() == 6);
    std::cout << nums << std::endl;

    {
        auto* n = nums.find(3);
        if(n) std::cout << n << " " << n->value << " " << n->next->value << std::endl;
    }

    assert(!nums.remove(10));
    assert(nums.size() == 6);
    assert(nums.remove(3));
    assert(nums.size() == 5);
    assert(!nums.find(3));

    assert(!nums.find(666));

    auto* n = nums.find(4);
    nums.insert(n, 44);
    assert(nums.size() == 6);

    nums.remove(nums.find(44));
    assert(!nums.find(44));
    assert(nums.size() == 5);

    std::cout << nums << std::endl;

    // insert cycle/loop
    {
        auto* n1 = nums.find(1);
        assert(n1->value == 1);
        auto* n2 = nums.find(4);
        assert(n2->value == 4);
        nums.insert(n1, n2);

        assert(nums.has_loop());
        assert(n2->value == 4);
        nums.remove(n1);
        assert(!nums.has_loop());
    }
}
