#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <array>
#include <limits>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <cassert>

#include "common.h"

// Write a program that will rotate a given array of size n by d elements. 
// Input  : 1 2 3 4 5 6 7 and d = 3 
// Output : 4 5 6 7 1 2 3
template <class ForwardIt>
void rotate(ForwardIt first, ForwardIt n_first, ForwardIt last)
{
    ForwardIt next = n_first;
    while (first != next) {
        std::iter_swap(first++, next++);
        if (next == last) {
            next = n_first;
        } else if (first == n_first) {
            n_first = next;
        }
    }
}

// Find kth largest element in an array. Elements in array are not sorted. 
// Input:  [1, 3, 12, 19, 13, 2, 15], k = 3 
// Output: 13
template<typename ForwardIt>
typename ForwardIt::value_type kthLargest(ForwardIt first, ForwardIt last, int k)
{
    typedef typename ForwardIt::value_type T;

    const auto n = last - first;

    std::priority_queue<T, std::vector<T>, std::greater<T>> minheap;
    for(int i=0; i<k && i<n; i++) 
        minheap.push(*(first+i));

    for(int i=k; i<n; i++) {
        auto it = first+i;
        if(*it > minheap.top()) {
            minheap.pop(); 
            minheap.push(*it);
        }
    }

    return minheap.top();
}

// Longest increasing subsequence
// Input: {10, 25, 9, 30, 21, 55, 40, 60, 75}
// Output: 6 i.e, [10,25,30,40,60,75] or [10,25,30,55,60,75]
/*template<typename ForwardIt>
int LongestIncreasingSubsequence(ForwardIt first, ForwardIt last)
{
}*/

// Make a function that shows the common elements in two arrays. 
// Part 2: With duplicates (small explanation). Part3: without duplicates 
//
// 1. Using a hash/set, O(A+B)
// 2.
std::vector<int> find_common_elements(const std::vector<int>& a, const std::vector<int>& b)
{
    std::unordered_set<int> in_a;
    for(int i=0; i<a.size(); i++)
    {
        in_a.insert(a[i]);
    }

    std::vector<int> common;
    for(int i=0; i<b.size(); i++)
    {
        if(in_a.find(b[i]) != in_a.end()) 
            common.push_back(b[i]);
    }

    return common;
}


// Given n*m fields of O's and X's, for example:
//
// OOOXOOO 
// OOXXOXO 
// OXOOOXO 
//
// Return the number of black shapes. A black shape consists of one or more adjacent X's (diagonals not included). In the example, the answer is 3. 
// Write code (I chose C++). What data structures would you use to store the input, and what to use for the shapes? What is the runtime complexity?
int count_contiguous(const std::vector<char>& s)
{
    int count = 0;
    int x = 0;
    for(int i=0; i<s.size(); i++)
    {
        if(s[i] == 'X') ++x;
        else 
        {
            if(x >= 2) ++count; 
            x = 0;
        }
    }
    count += x>=2;
    return count;
}

int count_contiguous(const std::string& s)
{
    return count_contiguous(std::vector<char>(s.begin(), s.end()));
}


int count_contiguous(int M, int N, const std::vector<std::vector<char>>& table, int column)
{
    int count = 0;
    int x = 0;
    for(int i=0; i<M; i++)
    {
        if(table[i][column] == 'X') ++x;
        else 
        {
            if(x >= 2) ++count; 
            x = 0;
        }
    }
    count += x>=2;
    return count;

}

/// table[row][col]
int count_shapes(int M, int N, const std::vector<std::vector<char>>& table)
{
    int count = 0;

    // O(M*N)
    for(int i=0; i<M; i++)
        count += count_contiguous(table[i]);

    // O(N*M)
    for(int i=0; i<N; i++)
        count += count_contiguous(M, N, table, i);


    // O(MN+NM) = O(2MN)
    return count;
}

int count_shapes_fast(int M, int N, const std::vector<std::vector<char>>& table)
{
    std::unordered_map<int, int> row_counts;
    std::unordered_map<int, int> col_counts;
    int count = 0;

    // O(MN) + |X| * constant lookups 
    for(int i=0; i<M; i++)
    {
        for(int j=0; j<N; j++)
        {
            if(table[i][j] == 'X')
            {
                // check for row shape
                if(i>0) 
                {
                    auto it = row_counts.find((i-1)+j*M);
                    if(it == row_counts.end())  // not in a row shape
                    {
                        row_counts[i+j*M] = 1;
                    }
                    else
                    {
                        // another 'x' to this shape
                        it->second += 1;
                        if(it->second == 2) count += 1; // count shapes *once* only 
                    }
                }
                else
                {
                    row_counts[i+j*M] = 1;
                }

                // check for column shape
                if(j>0)
                {
                    auto it = col_counts.find(i+(j-1)*M);
                    if(it == col_counts.end())  // not in a row shape
                    {
                        col_counts[i+j*M] = 1;
                    }
                    else
                    {
                        // another 'x' to this shape
                        it->second += 1;
                        if(it->second == 2) count += 1; // count shapes *once* only
                    }
                }
                else
                {
                    col_counts[i+j*M] = 1;
                }
            }
        }
    }

    return count;
}

// TODO: Write a method to generate a random number between 1 and 8, given a 
// method that generates a random number between 1 and 3

void test_shapes(std::initializer_list<std::string> rows, int expected_count)
{
    std::vector<std::vector<char>> table;

    for(auto row = rows.begin(); row != rows.end(); ++row)
        table.push_back(std::vector<char>(row->begin(), row->end()));

    int M = table.size();
    int N = table[0].size(); 

    auto count = count_shapes_fast(M, N, table);

    if(count != expected_count) 
        std::cout << M << "x" << N << " expected=" << expected_count << " actual=" << count << std::endl;

    assert(count == expected_count);
}

/*
Given an array of n numbers with repetition of numbers. Find the max length of 
continuous sub array with at max 3 unique elements.

For eg
    array: 1 2 3 1 4 3 4 1 2
    ans: 6 (3 1 4 3 4 1)

    Solution: Time complexity O(n)
    Extra Space O(1)
*/
int uniq_max_length(const std::vector<int>& a)
{
    int N = a.size();

    if(N <= 3) return N;

    int i=0;
    int j=1;
    int k=2;

    int p = k+1;
    for(; p<N; p++)
    {
        if(a[p] != a[i] && a[p] != a[j] && a[p] != a[k])
        {
            i++; j++; k++;
        }
    }

    if(k >= N) return 3;
    else return p-i; 
}

//  Find the palindrome of a given number without using extra space
int palindrome(int n)
{
    int p = 0;
    while(n > 0)
    {
        p = p * 10 + n % 10;
        n /= 10;
    }
    return p;
}

// You are given array of numbers which increasing first then decreasing. Find the greates number.
// eg: 1 2 3 4 5 4 3
// answer: 5
// time = O(logn)
// space = O(1)
int find_largest(const std::vector<int>& a, int lo, int hi)
{
    // 0+1 / 2 == 0  [0]
    // 0+2 / 2 == 1  [0,1]
    // [0,1,2,...]
    if(hi-lo <= 1) 
    {
        if(a[lo-1] > a[lo]) return a[lo-1];
        else return a[lo];
    }

    int mid = (lo+hi)/2;
    if(a[mid] > a[mid-1] ) 
    {
        // move right
        find_largest(a, mid+1, hi);
    }
    else 
    {
        // move left
        find_largest(a, lo, mid);
    }
}

int find_largest(const std::vector<int>& a)
{
    return find_largest(a, 0, a.size());
}

int main(int argc, char* argv[])
{
    using namespace std;
    cout << palindrome(321) << endl;
    assert(find_largest({1,2,3,4,5,4,3}) == 5);
    assert(find_largest({1,2,3,4,5,6,3}) == 6);
    assert(find_largest({1,2,3,4,5,6,7}) == 7);
    assert(find_largest({10,9,8,7}) == 10);

    //cout << uniq_max_length(make_vector({1,2,3,1,4,3,4,1,2})) << endl;
    return 0;

    vector<int> v = make_vector<int>({3,1,12,19,13,2,15});
    // Input:  [1, 3, 12, 19, 13, 2, 15], k = 3 
    //         [1, 3, 12, 19, 13, 2, 15], k = 3 
    //cout << kthLargest(v.begin(), v.end(), 3) << endl;
    //for(auto n = v.begin(); n != v.end(); ++n) cout << *n << ","; cout << endl;
    //cout << kthLargest(v.begin(), v.end(), 3) << endl;

    auto a = make_vector<int>({1,4,6,7,8});
    auto b = make_vector<int>({2,4,5,6,9});
    print(find_common_elements(a,b));

    assert(count_contiguous("OXXOXOXXXO") == 2);
    assert(count_contiguous("OXXOXOXXXX") == 2);
    assert(count_contiguous("OXXOXXOOOX") == 2);
    assert(count_contiguous("XOOOXXOOOX") == 1);
    assert(count_contiguous("XOOOOXOOOX") == 0);

    test_shapes({
        "OOOXOOO",
        "OOXXOXO",
        "OXOOOXO"
    }, 3);

    test_shapes({
        "OOOXOOO",
        "OOXXOXO",
        "OXOOOXO",
        "OXOOOXO"
    }, 4);

    test_shapes({
        "OOOXOOXX",
        "OOXXOOOO",
        "OXOOOOOX",
        "OXOOOXOX"
    }, 5);

}
