#include "priority_queue.h"

int main(int argc, char* argv[])
{
    using namespace std;

    PriorityQueue<int> queue;

    auto nums = make_vector({13,21,16,24,31,19,68,65,26,32});

    for(auto n = nums.begin(); n != nums.end(); ++n)
        queue.push(*n);

    nums.push_back(14);
    queue.push(14);

    auto sorted_nums = nums;
    std::sort(sorted_nums.begin(), sorted_nums.end());

    for(auto n = sorted_nums.begin(); n != sorted_nums.end(); ++n)
        assert(queue.pop() == *n);

    assert(queue.empty());
 
    queue.push(13);
    queue.push(14);
    queue.push(20);

    assert(queue.get_key(13) == 0);
    assert(queue.get_key(14) == 1);
    assert(queue.get_key(20) == 2);

    queue.increase(0, 15); // 13 -> 15
    assert(queue.top() == 14);

    assert(queue.get_key(14) == 0);
    assert(queue.get_key(20) == 1);
    assert(queue.get_key(15) == 2);

    queue.increase(0, 16); // 14 -> 16
    assert(queue.top() == 15);

    queue.push(10);
    assert(queue.top() == 10);

    queue.increase(1, 17); // 15 -> 17
    assert(queue.top() == 10);

    assert(is_heap(make_vector({13,14,16,24,21,19,68,65,26,32,31})));
    assert(!is_heap(make_vector({14,13,16,24,21,19,68,65,26,32,31})));

    assert( is_heap(make_vector({13})));
    assert( is_heap(make_vector({13,14})));
    assert( is_heap(make_vector({13,14,16})));

    assert(!is_heap(make_vector({13,10})));
    assert(!is_heap(make_vector({13,14,10})));

    assert( is_heap(make_vector({13,14,16,20})));
    assert(!is_heap(make_vector({13,14,16,10})));

    {
        auto nums = make_vector({3,6,7,5,3,5,6,2,9,1});
        build_heap(nums);
        assert(nums == make_vector({1,2,5,3,3,7,6,5,9,6}));
        assert(is_heap(nums));
    }

    srand(time(NULL));
    {
        auto N = 1000;
        std::vector<int> nums(N);
        for(int i=0; i<N; i++) nums[i] = rand() % N;
        build_heap(nums);
        assert(is_heap(nums));
    }

    return 0;
}
