#!/usr/bin/python

import random
import sys

N = 100000

words = open(sys.argv[1]).readlines()
for i in xrange(N):
    print words[random.randint(0,len(words)-1)].rstrip('\n')
