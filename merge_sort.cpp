#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <cassert>
#include <chrono>
#include <tgmath.h>

#include "common.h"

using std::make_pair;

bool is_sorted(const std::vector<int>& v, size_t begin, size_t end)
{
    for(size_t i=begin+1; i<end; i++)
    {
        if(v[i] < v[i-1]) return false;
    }
    return true;
}

bool is_sorted(const std::vector<int>& v)
{
    return is_sorted(v, 0, v.size());
}


static size_t ops = 0;


void merge(std::vector<int>& v, std::vector<int>& aux, size_t lo, size_t mid, size_t hi)
{
    assert(is_sorted(v, lo, mid));
    assert(is_sorted(v, mid, hi));

    std::copy(v.begin()+lo, v.begin()+hi, aux.begin()+lo);

    size_t i = lo;
    size_t j = mid;

    for(size_t k=lo; k<hi; k++)
    {
        ++ops;
        if      (i >= mid)          v[k] = aux[j++];   // this copying is unnecessary
        else if (j >= hi)           v[k] = aux[i++];
        else if (aux[j] < aux[i])   v[k] = aux[j++];
        else                        v[k] = aux[i++];
    }

    assert(is_sorted(v, lo, hi));
}

void sort(std::vector<int>& v, std::vector<int>& aux, size_t lo, size_t hi)
{
    if(hi-lo <= 1) return;
    else
    {
        auto mid = lo + (hi - lo) / 2;
        sort(v, aux, lo, mid);
        sort(v, aux, mid, hi);
        merge(v, aux, lo, mid, hi);
    }
}

void merge_sort(std::vector<int>& nums)
{
    ops = 0;
    auto N = nums.size();
    std::vector<int> aux(N);
    sort(nums, aux, 0, N);
    assert(is_sorted(nums));
}

void check_random(size_t N)
{
    std::vector<int> nums;
    nums.reserve(N);

    for(int i=0; i<N; i++) nums.push_back(rand()%N);

    Timer t;
    merge_sort(nums);
    auto elapsed = t.elapsed();
    std::cout << "merge_sort(" << N << ") = " << elapsed << "s" << " " << N / elapsed << " nlogn = " << N*log(N) << ", ops = " << ops << " (expected = " << N*pow(2,N) << ")" << std::endl;
    assert(is_sorted(nums));
}

int main()
{
    using namespace std;

    auto nums = make_vector({10,2,5,8,4,6,7});
    merge_sort(nums);
    assert(is_sorted(nums));

    srand(time(NULL));
    for(int i=0; i<16; i++) check_random(1 << i);
}
