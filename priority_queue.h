#pragma once

#include <iostream>
#include <string>
#include <array>
#include <deque>
#include <map>
#include <memory>
#include <algorithm>
#include <vector>
#include <set>
#include <limits>
#include <fstream>
#include <utility>
#include <tuple>
#include <cassert>
#include <sstream>
#include <map>
#include <stdexcept>
 
#include "common.h"

template<typename Container, typename Compare = std::less<typename Container::value_type>>
void percolate_down(Container& container, int i, size_t last, const Compare& cmp = Compare())
{
    while(i < last)
    {
        auto lhs = 2*i+1;
        auto rhs = 2*i+2;
        if(lhs >= last) return;    // no children
        if(rhs >= last)              // left child only
        {
            if(cmp(container[i], container[lhs]))  return;

            //std::cout << "lhs swap(" << lhs << "," << i << ")=" << container[lhs] << " <-> " << container[i] << std::endl;   
            std::swap(container[lhs], container[i]);
            i = lhs;
        }
        else                        // both children
        {
            size_t smallest_child = lhs;
            if(cmp(container[rhs], container[lhs])) smallest_child = rhs;
            if(cmp(container[i], container[smallest_child])) return;

            //std::cout << (smallest_child == lhs ? "lhs" : "rhs") << " swap(" << smallest_child << "," << i << ")=" << container[smallest_child] << " <-> " << container[i] << std::endl;   
            std::swap(container[smallest_child], container[i]);
            i = smallest_child;
        }
    }
}

template<
    typename T, 
    typename Container = std::vector<T>,
    typename Compare = std::less<typename Container::value_type>
>
class PriorityQueue
{
public:
    struct Element
    {
        T t;
        size_t index;
    };

    PriorityQueue(): m_array(128), m_last(-1) {}

    bool empty() const { return m_last < 0; }
    T top() const { return m_array[0]; }

    int resize()
    {
        // resize
        std::cout << "resizing queue!" << std::endl;
        Container new_array(m_array.size()*2);
        std::copy(m_array.begin(), m_array.end(), new_array.begin());
        //std::swap(m_array, new_array);

        return static_cast<int>(m_array.size());
    }

    void push(const T& t) 
    {
        auto N = static_cast<int>(m_array.size());

        if(m_last >= N) 
        {
            N = resize();
        }

        auto next = ++m_last;
        auto parent = get_parent(next);
        while(cmp(t, m_array[parent]) && next > 0)
        {
            swap(next, parent);
            next = parent;
            parent = get_parent(parent);
        }
        
        key_map[t] = next;
        m_array[next] = t;
    }

    int get_key(const T& t) 
    { 
        auto it = key_map.find(t);
        if(it == key_map.end()) return -1;
        return it->second;
    }

    void increase(int i, const T& t)
    {
        if(i < 0 || i > m_last)    throw std::runtime_error("key not found");
        //if(!cmp(m_array[i], newT))  throw std::runtime_error("new key less than existing key");

        m_array[i] = t;
        key_map[t] = i;
        if(!valid(i)) percolate_down(i);
    }

    bool valid(int i)
    {
        auto& v = m_array;
        auto& last = m_last;
        auto lhs = 2*i+1;
        auto rhs = 2*i+2;

        //std::cout << "valid: i=" << i << " last=" << last << " lhs=" << lhs << " rhs=" << rhs 
        //    << " v=" << v[i] << " lhs[]=" << v[lhs] << " rhs[]=" << v[rhs] << std::endl;

        if(i >= last || lhs > last) return true;                        // invalid or last node, or no children
        else if(lhs <= last && rhs > last) return cmp(v[i], v[lhs]);    // left child only
        else return cmp(v[i], v[lhs]) && cmp(v[i], v[rhs]);             // both children
    }

    void swap(int i, int j)
    {
        key_map[m_array[i]] = j;
        key_map[m_array[j]] = i;
        std::swap(m_array[i], m_array[j]);
    }

    void percolate_down(int i)
    {
        auto& v = m_array;
        auto& last = m_last;
        while(i >= 0 && i <= last)
        {
            auto lhs = 2*i+1;
            auto rhs = 2*i+2;

            if(lhs >= last || cmp(v[last], v[lhs]) && cmp(v[last], v[rhs])) {    
                // last fits int i (or i has no children) 
                swap(i, last);
                return;
            }

            if(cmp(v[lhs], v[rhs])) {
                swap(i, lhs);
                i = lhs;
            }
            else {
                swap(i, rhs);
                i = rhs;
            }
        }
    }

    T pop()
    {
        using namespace std;

        if(m_array.empty()) throw std::runtime_error("queue is empty!");

        size_t i = 0;
        auto top_value = m_array[i];
        percolate_down(i);
        m_last--;
        return top_value;
    }

    friend std::ostream& operator<<(std::ostream& out, const PriorityQueue<T, Container, Compare>& queue)
    {
        for(int i=0; i<=queue.m_last; i++)
            out << queue.m_array[i] << " ";
        return out;
    }

private:
    static size_t get_parent(size_t i) { return (i-1)/2; };

    Container       m_array;
    Compare         cmp;
    int             m_last;
    std::map<T,int> key_map;

};

template<typename Container>
void build_heap(Container& c)
{
    auto N = c.size();
    for(int i=N/2-1; i>=0; i--)
    {
        //std::cout << "build_heap: c[" << i << "]=" << c[i] << std::endl;
        percolate_down(c, i, N);
    }
}

template<typename Container>
bool is_heap(const Container& v)
{
    auto N = v.size();
    for(int i=0; i<N; i++)
    {
        auto lhs = 2*i+1;
        auto rhs = 2*i+2;

        if(lhs >= N) continue;    // no children
        if(rhs >= N)              // left child only
        {
            if(v[lhs] < v[i]) 
            {
                //std::cout << "is_heap: v[" << i << "]=" << v[i] << " N=" << N << " v[" << lhs << "]=" << v[lhs] 
                //    << " v[" << rhs << "]=n/a" << std::endl;
                return false;
            }
        }
        else                     // both children
        {
            if(v[lhs] < v[i] || v[rhs] < v[i]) 
            {
                //std::cout << "is_heap: v[" << i << "]=" << v[i] << " N=" << N << " v[" << lhs << "]=" << v[lhs] 
                //    << " v[" << rhs << "]=" << v[rhs] << std::endl;
                return false;
            }
        }
    }
    return true;
}
