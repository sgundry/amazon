#!/usr/bin/python

import random
import sys

N = int(sys.argv[1])
print '\n'.join(map(str, [random.randint(0,N) for n in xrange(N)]))

