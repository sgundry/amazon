#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <cassert>
#include <chrono>
#include <tgmath.h>

#include "common.h"

using std::make_pair;

static size_t ops = 0;

template<typename Container>
struct Median3Pivot
{
    static int pick(Container& s, int lo, int hi)
    {
        auto mid = (lo+hi) / 2;
        if(s[lo]  > s[mid])  std::swap(s[lo], s[mid]);
        if(s[lo]  > s[hi])   std::swap(s[lo], s[hi]);
        if(s[mid] > s[hi])   std::swap(s[mid], s[hi]);

        std::swap(s[mid], s[hi-1]); // hide pivot
        return s[hi-1];
    }
};

template<typename Container>
struct RandomPivot
{
    static int pick(Container& s, int lo, int hi)
    {
        auto p = randint(lo, hi);
        auto pivot = s[p];
        std::swap(s[p], s[hi-1]); // hide pivot
        return pivot; 
    }
};

template<typename Container, typename PickPivot = RandomPivot<Container>>
void quick_sort(Container& s, int lo, int hi)
{
    auto cutoff = 1;
    if(lo + cutoff >= hi) 
    {
        assert(cutoff <= 1); // nothing to sort
        return; 
    }
    else
    {
        auto pivot = PickPivot::pick(s, lo, hi); 

        int i = lo-1;
        int j = hi-1;
        while(true)
        {
            while(s[++i] < pivot);
            while(s[--j] > pivot);
            if(i < j) 
            {
                std::swap(s[i], s[j]);
            }
            else
            {
                break;
            }
        }
        std::swap(s[i], s[hi-1]); // restore pivot

        quick_sort(s, lo, i);
        quick_sort(s, i+1, hi);
    }
}

template<typename Container, typename PickPivot = RandomPivot<Container>>
void quick_sort(Container& s)
{
    quick_sort<Container, PickPivot>(s, 0, s.size());
}

void check_random(size_t N)
{
    std::vector<int> nums;
    nums.reserve(N);

    for(int i=0; i<N; i++) nums.push_back(rand()%N);

    Timer t;
    //quick_sort<decltype(nums), Median3Pivot<decltype(nums)>>(nums);
    quick_sort<decltype(nums), RandomPivot<decltype(nums)>>(nums);
    auto elapsed = t.elapsed();
    std::cout << "merge_sort(" << N << ") = " << elapsed << " seconds" << std::endl;
    assert(is_sorted(nums));
}

template<typename Container, typename PickPivot = RandomPivot<Container>>
void quick_select(Container& s, int k, int lo, int hi)
{
    if(hi-lo <= 1) return;
    else
    {
        auto pivot = PickPivot::pick(s, lo, hi);

        int i = lo-1;
        int j = hi-1;

        while(true)
        {
            while(s[++i] < pivot);
            while(s[--j] > pivot);
            if(i < j) std::swap(s[i], s[j]);
            else break;
        }
        std::swap(s[i], s[hi-1]);

        // quick_select differs to quick_sort here. 
        if(k <= i) quick_select(s, k, lo, i);
        else if(k > i+1) quick_select(s, k, i+1, hi);
    }
}

template<typename Container, typename PickPivot = RandomPivot<Container>>
typename Container::value_type quick_select(Container& s, int k)
{
    quick_select<Container, PickPivot>(s, k, 0, s.size());
    return s[k-1];
}

int main()
{
    using namespace std;

    {
        auto nums = make_vector({8,1,4,9,0,3,5,2,7,6});
        quick_sort<decltype(nums), Median3Pivot<decltype(nums)>>(nums);
        assert(is_sorted(nums));
    }

    {
        auto nums = make_vector({8,1,4,9,0,3,5,2,7,6});
        quick_sort<decltype(nums), RandomPivot<decltype(nums)>>(nums);
        assert(is_sorted(nums));
    }

    {
        auto nums = make_vector({8,1,4,9,0,3,5,2,7,6});
        assert(quick_select(nums,0) == 0);
        assert(quick_select(nums,1) == 0);
        assert(quick_select(nums,2) == 1);
        assert(quick_select(nums,9) == 8);
        assert(quick_select(nums,10) == 9);
    }

    //srand(time(NULL));
    //for(int i=0; i<16; i++) check_random(1028 << i);
}
