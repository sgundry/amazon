#include <iostream>

template<typename T>
class BinaryTree
{
    void insert(const T& t)
    {
        auto node = m_root;
        while(node && t < node->value) node = node.left
        while(node && t > node->value) node = node.right


    }

private: 
    struct Node 
    {
        T                     value;
        std::unique_ptr<Node> left;
        std::unique_ptr<Node> right;
    };

    std::unique_ptr<Node>       m_root;
};

int main(int argc, char* argv[])
{

    auto tree = BinaryTree();

    tree.insert(10);
    tree.insert(5);
    tree.insert(6);
    tree.insert(4);
    tree.insert(20);
    tree.insert(25);
    tree.insert(15);

    return 0;
}
