#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cassert>

#include "common.h"

using namespace std;

vector<int> kth_largest(const vector<int> nums, int k = 100)
{
    auto cmp = std::greater<int>();
    vector<int> minheap(nums.begin(), nums.begin()+k);
    make_heap(minheap.begin(), minheap.end(), cmp);
    pop_heap(minheap.begin(), minheap.end(), cmp);

    auto N = nums.size();
    for(int i=k; i<N; i++)
    {
        if(nums[i] > minheap.back()) 
        {
            minheap.pop_back();
            minheap.push_back(nums[i]);
            push_heap(minheap.begin(), minheap.end(), cmp);
            pop_heap(minheap.begin(), minheap.end(), cmp);
        }
    }

    sort_heap(minheap.begin(), minheap.end(), cmp);
    return minheap;
}

int main(int argc, char* argv[])
{
    auto N = std::atoi(argv[1]);
    auto k = std::atoi(argv[2]);

    std::vector<int> nums = make_random_vector(N, 100, 1000);
    //string n;
    //while(cin >> n) nums.push_back(std::atoi(n.c_str()));

    Timer timer;
    auto largest = kth_largest(nums, k);
    cout << "kth_largest: N=" << N << " k=" << k << " " << timer.elapsed() << " seconds" << endl;


    {
        Timer timer;
        std::vector<int> minheap(nums.begin(), nums.end());
        make_heap(minheap.begin(), minheap.end(), std::greater<int>());
        sort_heap(minheap.begin(), minheap.end(), std::greater<int>());
        cout << "make_heap: N=" << N << " k=" << k << " " << timer.elapsed() << " seconds" << endl;

        for(int i=0; i<k; i++) 
        {
            //cout << minheap[i] << " " << largest[i] << endl;
            assert(minheap[i] == largest[i]);
        }

    }

    return 0;
}
