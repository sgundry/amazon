#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

int last_bit(int x) { return (x & -x); }
int is_divisible_by_8(int x) { return !(x & 7); } 
int count_bits(int x)
{
    int count = 0;
    for(; x; count++) 
    {
        x &= x-1;
    }
    return count;
}

// Write a program to convert a decimal number into binary your code should work on both big endian and small endian machine. U have given a variable which tell u whether machine is big endian or small endian

// Given a floor of dimensions 2 x W and tiles of dimensions 2 x 1, write code to find the number of ways the floor can be tiled.
int count_tiles(int w)
{
    std::vector<int> sums(w+1);
    sums[0] = 0;
    sums[1] = 1;
    sums[2] = 2;
    
    for(int i=3; i<=w; i++)
    {
        sums[i] = sums[i-1] + sums[i-2];
    }
    
    return sums[w];
}

int main(int argc, char* argv[])
{
    assert(is_divisible_by_8(64));
    assert(!is_divisible_by_8(65));

    assert(count_bits(0) == 0);
    assert(count_bits(1) == 1);
    assert(count_bits(2) == 1);
    assert(count_bits(3) == 2);
    assert(count_bits(4) == 1);
    assert(count_bits(7) == 3);

    std::cout << count_tiles(0) << std::endl;
    std::cout << count_tiles(1) << std::endl;
    std::cout << count_tiles(2) << std::endl;
    std::cout << count_tiles(3) << std::endl;
    std::cout << count_tiles(4) << std::endl;
    std::cout << count_tiles(5) << std::endl;
    return 0;
}
