#include <iostream>
#include <string>
#include <memory>
#include <cassert>
#include <list>
#include <set>
#include <vector>
#include <stdexcept>
#include <functional>
#include <sstream>
#include <deque>
#include <unordered_set>
#include <limits>

#include "common.h"

template<typename T>
class BinarySearchTree
{
public:

    struct Node
    {
        Node(const T& t, Node* parent = NULL): value(t), nvalues(1), N(0), lhs(NULL), rhs(NULL), parent(parent) {}

        T value;
        size_t nvalues;
        size_t N;
        Node* lhs;
        Node* rhs;
        Node* parent;
    };


    BinarySearchTree(Node* root = NULL): root(root), ops(0) {}
    ~BinarySearchTree()
    {
        make_empty(root);
        assert(!root);
    }

    void make_empty(Node*& node)
    {
        if(!node) return;
        if(node->lhs) make_empty(node->lhs);
        if(node->rhs) make_empty(node->rhs);
        delete node;
        node = NULL;
    }

    void insert(const T& t)
    {
        if(root) insert(root, NULL, t);
        else root = new Node(t);
    }

    void remove(const T& t)
    {
        root = remove(root, t);
    }

    Node* remove(Node*& node, const T& t)
    {
        if(!node) return NULL;

        if(t < node->value)      node->lhs = remove(node->lhs, t);
        else if(t > node->value) node->rhs = remove(node->rhs, t);
        else
        {
            if(--node->nvalues > 0)
            {
                // still values left in node
                return node;
            }
            else
            {
                if(!node->lhs)
                {
                    Node* n = node->rhs;
                    std::swap(node, n);
                    delete n;
                }
                else if(!node->rhs)
                {
                    Node* n = node->lhs;
                    std::swap(node, n);
                    delete n;
                }
                else
                {
                    // node has both children
                    Node* n = findMin(node->rhs);
                    n->rhs = node->rhs;
                    n->lhs = node->lhs;
                    std::swap(node, n);
                    delete n;
                }
            }
        }

        if(node) node->N = 1 + size(node->lhs) + size(node->rhs);
        return node;
    }

    const Node* const find(const T& t) const
    {
        if(root) return find(root, t);
        else return NULL;
    }

    const Node* const find(Node* root, const T& t) const
    {
        if(!root || t == root->value)   return root;
        else if(t < root->value)        return find(root->lhs, t);
        else                            return find(root->rhs, t);
    }

    Node* getRoot() const { return root; }

    Node* findMin(Node* node = NULL) const
    {
        auto* n = node ? node : root;
        while(n->lhs) n = n->lhs;
        return n;
    }

    const Node* const findMax() const
    {
        auto* n = root;
        while(n->rhs) n = n->rhs;
        return n;
    }

    template<typename OnVisitNode>
    void traverse_inorder(const OnVisitNode& onVisitNode, const Node* const node = NULL) const
    {
        traverse_inorder(node ? node : root, onVisitNode);
    }

    template<typename OnVisitNode>
    void traverse_preorder(const OnVisitNode& onVisitNode, const Node* const node = NULL) const
    {
        traverse_preorder(node ? node : root, onVisitNode);
    }

    template<typename OnVisitNode>
    void traverse_postorder(const OnVisitNode& onVisitNode, const Node* const node = NULL) const
    {
        traverse_postorder(node ? node : root, onVisitNode);
    }

    template<typename OnVisitNode>
    void traverse_level(const OnVisitNode& onVisitNode, const Node* const node = NULL) const
    {
        traverse_level(node ? node : root, onVisitNode);
    }

    size_t size() const
    {
        return size(root);
    }

    size_t size(Node* node) const
    {
        if(!node) return 0;
        return node->N;
    }

    const Node* find_parent_slow(const Node* a, const Node* b) const
    {
        if(!a || !b) return NULL;

        // worst case traverses to root twice
        if (is_parent(b, a->parent)) return a->parent;
        else if(is_parent(a, b->parent)) return b->parent;
        else
        {
            return find_parent_slow(a->parent, b->parent);
        }
    }

    const Node* find_parent(const Node* a, const Node* b) const
    {
        if(!root) return NULL;
        else find_parent(a, b, root);
    }

    const Node* find_parent(const Node* a, const Node* b, const Node* top) const
    {
        if(top->value > a->value && top->value > b->value) return find_parent(a, b, top->lhs);
        else if(top->value < a->value && top->value < b->value) return find_parent(a, b, top->rhs);
        else return top;
    }

    // without using a root.
    const Node* find_parent_no_root(const Node* a, const Node* b) const
    {
        if(!a || !b) return NULL;
        if(a->value == b->value) return a;
        if(a->value > b->value) std::swap(a,b);

        auto* p = a;
        while(p && p->parent && p->parent->value <= b->value)
        {
            ops++;
            //std::cout << "p=" << p->value << " b=" << b->value << std::endl;
            p = p->parent;
        }

        if(!p) return NULL;
        if(p->value == b->value) return p;

        auto* q = b;
        //std::cout << "--" << std::endl;
        while(q && q->value > p->value)
        {
            ops++;
            //std::cout << "p=" << p->value << " q=" << q->value << std::endl;
            q = q->parent;
        }

        //std::cout << "done p=" << p->value << " q=" << q->value << std::endl;
        if(!q) return NULL;
        if(q->value == p->value) return q;
    }

    mutable size_t ops;

    bool is_parent(const Node* node, const Node* parent) const
    {
        while(node->parent && node->parent != parent)
        {
            ops++;
            node = node->parent;
        }
        return node->parent;
    }

private:
    void insert(Node*& node, Node* parent, const T& t)
    {
        if(!node)                   node = new Node(t, parent);
        else if(t < node->value)    insert(node->lhs, node, t);
        else if(t > node->value)    insert(node->rhs, node, t);
        else                      ++node->nvalues;
        node->N = 1 + size(node->lhs) + size(node->rhs);
    }

    template<typename OnVisitNode>
    void traverse_inorder(const Node* const node, const OnVisitNode& onVisitNode) const
    {
        onVisitNode.pre();
        if(node)
        {
            traverse_inorder(node->lhs, onVisitNode);
            onVisitNode(*node);
            traverse_inorder(node->rhs, onVisitNode);
        }
        onVisitNode.post();
    }

    template<typename OnVisitNode>
    void traverse_preorder(const Node* const node, const OnVisitNode& onVisitNode) const
    {
        onVisitNode.pre();
        if(node)
        {
            onVisitNode(*node);
            traverse_preorder(node->lhs, onVisitNode);
            traverse_preorder(node->rhs, onVisitNode);
        }
        onVisitNode.post();
    }

    template<typename OnVisitNode>
    void traverse_postorder(const Node* const node, const OnVisitNode& onVisitNode) const
    {

        onVisitNode.pre();
        if(node)
        {
            traverse_postorder(node->lhs, onVisitNode);
            traverse_postorder(node->rhs, onVisitNode);
            onVisitNode(*node);
        }
        onVisitNode.post();
    }


    template<typename OnVisitNode>
    void traverse_level(const Node* const node, const OnVisitNode& onVisitNode) const
    {
        if(!node) return;

        std::deque<const Node*> nodes_to_visit;

        nodes_to_visit.push_back(node);
        while(!nodes_to_visit.empty())
        {
            auto* n = nodes_to_visit.front();
            nodes_to_visit.pop_front();
            onVisitNode.pre();
            onVisitNode(*n);
            onVisitNode.post();
            if(n->lhs) nodes_to_visit.push_back(n->lhs);
            if(n->rhs) nodes_to_visit.push_back(n->rhs);
        }
    }

    Node*       root;
};

#ifndef NDEBUG
#   define ASSERT(condition, message) \
        do { \
                    if (! (condition)) { \
                                    std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                                              << " line " << __LINE__ << ": " << message << std::endl; \
                                    std::exit(EXIT_FAILURE); \
                                } \
                } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void check(const BinarySearchTree<int>& tree, const std::initializer_list<int>& hasValues, const std::initializer_list<int>& missingValues, int line)
{
    for(auto v = hasValues.begin(); v != hasValues.end(); ++v)
        ASSERT(tree.find(*v), std::to_string((long long)*v) + " not found (line " + std::to_string((long long)line) + ")");

    for(auto v = missingValues.begin(); v != missingValues.end(); ++v)
        ASSERT(!tree.find(*v), std::to_string((long long)*v) + "found (line + " + std::to_string((long long)line) + ")");
}

template<typename Out>
struct Visitor
{
    Visitor(Out& out): out(out) {}
    void pre() const { out << "["; }
    void post() const { out << "]"; }
    void operator()(const BinarySearchTree<int>::Node& node) const
    {
        out << node.value;
        for(int i=1; i<node.nvalues; i++) out << "," << node.value;
    }

    Out& out;
};

template<typename Out>
Visitor<Out> make_visitor(Out& out) { return Visitor<Out>(out); }

void test_find_parent()
{
    BinarySearchTree<int> tree;

    tree.insert(20);
    tree.insert(5);
    tree.insert(7);
    tree.insert(6);
    tree.insert(8);
    tree.insert(3);
    tree.insert(4);
    tree.insert(2);
    tree.insert(1);
    tree.insert(9);
    tree.insert(10);
    tree.insert(11);
    tree.insert(22);
    tree.insert(21);

    {
        assert(tree.find_parent_no_root(tree.find(1), tree.find(11)) == tree.find(5));
        assert(tree.find_parent(tree.find(1), tree.find(11)) == tree.find(5));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(7)) == tree.find(5));
        assert(tree.find_parent(tree.find(4), tree.find(7)) == tree.find(5));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(20)) == tree.find(20));
        assert(tree.find_parent(tree.find(4), tree.find(20)) == tree.find(20));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(5)) == tree.find(5));
        assert(tree.find_parent(tree.find(4), tree.find(5)) == tree.find(5));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(6)) == tree.find(5));
        assert(tree.find_parent(tree.find(4), tree.find(6)) == tree.find(5));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(22)) == tree.find(20));
        assert(tree.find_parent(tree.find(4), tree.find(22)) == tree.find(20));

        assert(tree.find_parent_no_root(tree.find(4), tree.find(21)) == tree.find(20));
        assert(tree.find_parent(tree.find(4), tree.find(21)) == tree.find(20));
    }
}

typedef BinarySearchTree<int>::Node Node;
bool is_bst(const Node* x, int low, int high)
{
    if(!x) return true;

    if(x->value > low && x->value <= high)
    {
        return is_bst(x->lhs, low, x->value)
            && is_bst(x->rhs, x->value, high);
    }
    else
    {
        return false;
    }
}

bool is_bst(const Node* root)
{
    if(!root) return true;
    return is_bst(root->lhs, std::numeric_limits<int>::min(), root->value) &&
           is_bst(root->rhs, root->value, std::numeric_limits<int>::max());
}

Node* sorted_list_to_bst(std::list<int>::const_iterator& list, int start, int end) 
{
  if (start > end) return NULL;

  // same as (start+end)/2, avoids overflow
  int mid = start + (end - start) / 2;
  auto *leftChild = sorted_list_to_bst(list, start, mid-1);
  auto *parent = new Node(*list);
  parent->lhs = leftChild;
  ++list;
  parent->rhs = sorted_list_to_bst(list, mid+1, end);
  return parent;
}

BinarySearchTree<int> sorted_list_to_bst(const std::list<int>& list)
{
    int N = list.size();  // O(n) until c++11, O(1) since c++11
    auto it = list.begin();
    auto* root = sorted_list_to_bst(it, 0, N-1);
    BinarySearchTree<int> tree(root);
    return tree;
}

Node* sorted_array_to_bst(const std::vector<int>& a, int lo, int hi)
{
    assert(is_sorted(a));

    if(hi-lo <= 0) return NULL;
    else 
    {
        auto mid = (lo+hi)/2;
        auto* node = new Node(a[mid]);
        node->lhs = sorted_array_to_bst(a, lo, mid);
        node->rhs = sorted_array_to_bst(a, mid+1, hi);
        return node;
    }
}

BinarySearchTree<int> sorted_array_to_bst(const std::vector<int>& a)
{
    assert(is_sorted(a));
    auto* root = sorted_array_to_bst(a, 0, a.size());
    BinarySearchTree<int> tree(root);
    return tree;
}

void test_array_to_bst()
{
    auto nums = make_vector<int>({1,2,3,4,5,6,7,8});
    auto tree = sorted_array_to_bst(nums);
    assert(is_bst(tree.getRoot()));

    std::stringstream ss;
    tree.traverse_preorder(make_visitor(ss));
    std::cout << "array_to_bst: " << ss.str() << std::endl;
    assert(is_bst(tree.getRoot()));
}

void test_list_to_bst()
{
    auto v = make_vector<int>({1,2,3,4,5,6,7,8});
    std::list<int> list(v.begin(), v.end());
    auto tree = sorted_list_to_bst(list);

    std::stringstream ss;
    tree.traverse_inorder(make_visitor(ss));
    std::cout << ss.str() << std::endl;
    assert(is_bst(tree.getRoot()));

}

const Node* find_lca(const Node* a, const Node* b)
{
    std::unordered_set<const Node*> visited;
    while(!a || !b) // until both are null
    {
        if(a) 
        {
            if(visited.find(a) != visited.end()) return a;
            else 
            {
                visited.insert(a); 
                a = a->parent;
            }
        }

        if(b)
        {
            if(visited.find(b) != visited.end()) return b;
            else
            {
                visited.insert(b);
                b = b->parent;
            }
        }
    }
    return NULL;
}

void test_find_lca()
{
    auto nums = make_vector<int>({1,2,3,4,5,6,7,8});
    auto tree = sorted_array_to_bst(nums);
    assert(is_bst(tree.getRoot()));
    

    std::stringstream ss;
    tree.traverse_inorder(make_visitor(ss));
    std::cout << ss.str() << std::endl;
    assert(tree.find(5));
    assert(tree.find(5)->lhs);
    assert(tree.find(5)->lhs->value == 3);
    assert(tree.find_parent(tree.find(1), tree.find(8)) == tree.find(5));
}

void test_is_bst()
{
    BinarySearchTree<int> tree;
    tree.insert(20);
    tree.insert(10);
    tree.insert(30);
    tree.insert(3);
    tree.insert(15);
    tree.insert(5);
    tree.insert(40);

    assert(is_bst(tree.getRoot()));

    {
        Node* root = new Node(20);
        root->lhs = new Node(10);
        root->rhs = new Node(30);
        root->lhs->lhs = new Node(3);
        root->lhs->rhs = new Node(15);
        root->rhs->lhs = new Node(5); // invalidates BST  (on zig-zag GP.rhs)
        root->rhs->rhs = new Node(40);

        assert(!is_bst(root));

        delete root->rhs->rhs;
        delete root->rhs->lhs;
        delete root->lhs->rhs;
        delete root->lhs->lhs;
        delete root->rhs;
        delete root->lhs;
        delete root;
    }

    {
        Node* root = new Node(20);
        root->lhs = new Node(10);
        root->rhs = new Node(30);
        root->lhs->lhs = new Node(3);
        root->lhs->rhs = new Node(25); // invalidates BST  (on zig-zag GP.lhs)
        root->rhs->lhs = new Node(21);
        root->rhs->rhs = new Node(40);

        assert(!is_bst(root));

        delete root->rhs->rhs;
        delete root->rhs->lhs;
        delete root->lhs->rhs;
        delete root->lhs->lhs;
        delete root->rhs;
        delete root->lhs;
        delete root;
    }
}


int main(int argc, char* argv[])
{
    using namespace std;

    test_find_parent();
    test_is_bst();
    test_array_to_bst();
    test_list_to_bst();
    test_find_lca();

    BinarySearchTree<int> tree;

    tree.insert(6);
    tree.insert(4);
    tree.insert(5);

    assert(tree.size() == 3);
    check(tree, {4,5,6}, {7}, __LINE__);

    {
        std::stringstream ss;
        tree.traverse_inorder(make_visitor(ss));
        assert(ss.str() == "[[[]4[[]5[]]]6[]]");
    }

    {
        std::stringstream ss;
        tree.traverse_preorder(make_visitor(ss));
        assert(ss.str() == "[6[4[][5[][]]][]]");
    }

    {
        std::stringstream ss;
        tree.traverse_postorder(make_visitor(ss));
        assert(ss.str() == "[[[][[][]5]4][]6]");
    }

    {
        auto* n = tree.find(6);
        assert(n->lhs->value == 4);
        assert(n->rhs == NULL);
        assert(n->parent == NULL);
    }
    {
        auto* n = tree.find(4);
        assert(n->lhs == NULL);
        assert(n->rhs->value == 5);
        assert(n->parent->value == 6);
    }

    tree.remove(5);
    assert(tree.size() == 2);
    assert(!tree.find(5));
    check(tree, {4,6}, {5}, __LINE__);

    {
        auto* n = tree.find(4);
        assert(n->lhs == NULL);
        assert(n->rhs == NULL);
    }

    tree.insert(5);
    assert(tree.size() == 3);
    check(tree, {4,5,6}, {}, __LINE__);

    {
        auto* n = tree.find(4);
        assert(n->lhs == NULL);
        assert(n->rhs->value == 5);
        assert(n->parent->value == 6);
    }

    assert(tree.findMin()->value == 4);
    assert(tree.findMax()->value == 6);

    {
        std::stringstream ss;
        tree.traverse_level(make_visitor(ss));
        assert(ss.str() == "[6][4][5]");
    }

    tree.remove(6);
    assert(tree.size() == 2);
    check(tree, {4,5}, {6}, __LINE__);

    assert(tree.findMin()->value == 4);
    assert(tree.findMax()->value == 5);

    tree.insert(3);
    tree.insert(8);
    assert(tree.size() == 4);

    assert(tree.findMin()->value == 3);
    assert(tree.findMax()->value == 8);

    tree.insert(8);
    assert(tree.size() == 4);
    assert(tree.find(8)->value == 8);
    assert(tree.find(8)->nvalues == 2);

    {
        std::stringstream ss;
        tree.traverse_preorder(make_visitor(ss));
        assert(ss.str() == "[4[3[][]][5[][8,8[][]]]]");
    }

    {
        std::stringstream ss;
        tree.traverse_level(make_visitor(ss));
        assert(ss.str() == "[4][3][5][8,8]");
    }

    tree.remove(8);
    assert(tree.size() == 4);
    assert(tree.find(8)->value == 8);
    assert(tree.find(8)->nvalues == 1);

    tree.remove(8);
    assert(!tree.find(8));
    assert(tree.size() == 3);
}
